import { Router } from '@vaadin/router';
import  '../../Views/Home/home-container'
import {checkRouterAccess} from '../@guard/AuthGuard'

export function initRouter() {
    const router = new Router(document.querySelector('main')); 
  
  
    router.setRoutes([
      {
        path: '/',
        component: 'login-view'
      },
      {
        path: '/Home',
        component:'home-container',
        children: () => import('./users-routes.js').then(module => module.default),
        action:checkRouterAccess,
      },
      {
        path: '(.*)', 
        component: 'not-found-view',
        action: () =>
          import(/* webpackChunkName: "not-found-view" */ '../../Views/Notfound/not-found-view')
      }
    ]);
  }